const express = require('express')
const path = require('path')
const app = express()
const chat = require('./routes/chat.cjs')

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.send({
        "statusCode": 200,
        "Message": "Success"
    })
})

app.use('/chat',chat)
app.listen(process.env.PORT || 3000, () => {
    console.log(`Server listening on port ${process.env.PORT || 3000}`)
})