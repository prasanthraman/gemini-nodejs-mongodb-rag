const fs = require('fs')
require('dotenv').config()
let PdfParse = require("pdf2json");
const parser = new PdfParse(this, 1);
const createEmbeddings = require('../utils/createEmbeddings.cjs')

const { MongoClient, ObjectId } = require("mongodb");
const DB = process.env.MONGO_CONNECTION_URL

try {
    parser.loadPDF("./docs/test.pdf");
    parser.on("pdfParser_dataReady", async (data) => {
        fs.writeFileSync("./context.txt", parser.getRawTextContent());
        const content = fs.readFileSync("./context.txt", "utf-8");
        const splitContent = content.split("\n");
        const connection = await MongoClient.connect(DB);
        if (connection) {
            console.log("Making db conn");
        }
        else {
            console.log("Mongo connection failure")
        }
        const db = connection.db("rag_doc");
        const collection = db.collection("demo");

        for (line of splitContent) {
            const embedings = await createEmbeddings(line);
            await collection.insertOne({
                text: line,
                // embeddings : embedings.data[0].embeding
                embeddings: embedings,
            });
        }
        await connection.close();
        console.log("done")
    });
} catch (error) {
    console.log(error);
    res.json(500).json({ message: "Error" });
}