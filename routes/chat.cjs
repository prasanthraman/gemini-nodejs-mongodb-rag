const fs = require("fs");
const express = require('express')
const router = express.Router()

require('dotenv').config()
const createEmbeddings = require('../utils/createEmbeddings.cjs')
const { MongoClient, ObjectId } = require("mongodb");
const { GoogleGenerativeAI } = require("@google/generative-ai");
const { error } = require("console");
const genAI = new GoogleGenerativeAI(process.env.GEMINI_API_KEY);
const DB = process.env.MONGO_CONNECTION_URL



router.post('/', async (req, res) => {
    try {
        console.log(req.body)
        const connection = await MongoClient.connect(DB);
        if (connection) {
            console.log("Making db conn");
        }
        else {
            console.log("Mongo connection failure")
        }
        const db = connection.db("rag_doc");
        const messageVector = await createEmbeddings(req.body.message);
    
        const demoCollection = db.collection("demo");
        const vectorSearch = demoCollection.aggregate([
            {
                $vectorSearch: {
                    index: "default",
                    path: "embeddings",
                    queryVector: messageVector,
                    numCandidates: 150,
                    limit: 10,
                },
            },
            {
                $project: {
                    _id: 0,
                    text: 1,
                    score: {
                        $meta: "vectorSearchScore",
                    },
                },
            },
        ]);
    
        let finalResult = []
    
    
        for await (let doc of vectorSearch) {
            finalResult.push(doc)
        }
    
    
        const model = genAI.getGenerativeModel({ model: "gemini-pro" });
        console.log(finalResult)
        finalResult = finalResult[0]
        // const formattedContent = finalResult.map(doc => doc.text).join("\n");
        // Combine with the additional question
        const formattedmessage = `${finalResult.text}\n\n From the above context, answer the following question: ${req.body.message}`;
    
        console.log(formattedmessage)
        const chat = model.startChat({
            history: [
                {
                    role: "user",
                    parts: formattedmessage
                },
                {
                    role: "model",
                    parts: "You are a humble helper who can answer for questions asked by users from the given context.",
    
                },
    
            ],
            generationConfig: {
                maxOutputTokens: 100,
            },
        });
    
        // const msg = "Who is thala?";
        // console.log("users messsage", message);
    
        const result = await chat.sendMessage(req.body.message);
        const response = await result.response;
        const text = response.text();
        console.log(text);
        res.json(text);

    }
    catch (error) 
    {
        console.log(error)
        res.send("Something went wrong")
    }
 

})

module.exports = router
