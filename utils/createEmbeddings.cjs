const { GoogleGenerativeAI } = require("@google/generative-ai");
const genAI = new GoogleGenerativeAI(process.env.GEMINI_API_KEY);

async function createEmbeddings(text) {
    try {
        console.log("Create embedding function", text);
        const model = genAI.getGenerativeModel({ model: "embedding-001" });

        const result = await model.embedContent(text);
        const embedding = result.embedding.values;
        return embedding;
    } catch (error) {
        throw new Error(error);
    }
}

module.exports = createEmbeddings;